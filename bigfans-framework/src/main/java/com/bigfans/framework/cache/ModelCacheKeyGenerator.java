package com.bigfans.framework.cache;

import java.lang.reflect.Method;

import com.bigfans.framework.model.AbstractModel;

/**
 * 
 * @Title: 
 * @Description: 按照Model的ID生成key值
 * @author lichong 
 * @date 2016年1月17日 上午11:10:29 
 * @version V1.0
 */
public class ModelCacheKeyGenerator implements CacheKeyGenerator {
	
	private static final String SEPERATOR = ".";
	
	@Override
	public String generate(Object target, Method method, Object... params) {
		StringBuilder key = new StringBuilder(20);
		for (Object object : params) {
			if(object instanceof AbstractModel){
				key.append(((AbstractModel) object).getModule());
				key.append(SEPERATOR);
				key.append(((AbstractModel) object).getId());
			}
		}
		return key.toString();
	}


}
