package com.bigfans.framework.es.request;

import java.io.Serializable;

/**
 * 
 * @Description:
 * @author lichong 2015年2月9日上午10:09:02
 *
 */
public class MoreLikeThisSearchCriteria extends AbstractSearchCriteria implements Serializable{

	private static final long serialVersionUID = -865280434038864020L;
	
	private String[] fields;
	private String likeText;
	private int minTermFreq = 1;
	private int minDocFreq = 1;
	private int maxQueryTerm = 12;
	private String sourceDocId;

	public String[] getFields() {
		return fields;
	}

	public void setFields(String... fields) {
		this.fields = fields;
	}

	public String getLikeText() {
		return likeText;
	}

	public void setLikeText(String likeText) {
		this.likeText = likeText;
	}

	public int getMinTermFreq() {
		return minTermFreq;
	}

	public void setMinTermFreq(int minTermFreq) {
		this.minTermFreq = minTermFreq;
	}

	public int getMaxQueryTerm() {
		return maxQueryTerm;
	}

	public void setMaxQueryTerm(int maxQueryTerm) {
		this.maxQueryTerm = maxQueryTerm;
	}

	public int getMinDocFreq() {
		return minDocFreq;
	}

	public void setMinDocFreq(int minDocFreq) {
		this.minDocFreq = minDocFreq;
	}

	public String getSourceDocId() {
		return sourceDocId;
	}

	public void setSourceDocId(String sourceDocId) {
		this.sourceDocId = sourceDocId;
	}
}
