package com.bigfans.framework.es;

/**
 * 
 * @author lichong
 *
 */
public interface Mapping {

	Object getMapping();
	
	String getIndex();
	
	String getType();
	
}
