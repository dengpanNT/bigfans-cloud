package com.bigfans.paymentservice.service.impl;

import com.bigfans.framework.redis.JedisConnection;
import com.bigfans.framework.redis.JedisExecuteCallback;
import com.bigfans.framework.redis.JedisExecuteWithoutReturnCallback;
import com.bigfans.framework.redis.JedisTemplate;

/**
 * @author lichong
 * @create 2018-04-09 下午8:43
 **/
public class RedisService {

    private JedisTemplate jedisTemplate;

    private static final String ORDER_QRIMG_PREFIX = "order:qrimg:";

    public RedisService(JedisTemplate jedisTemplate) {
        this.jedisTemplate = jedisTemplate;
    }

    public void setOrderQrImg(String orderId, String imgPath) {
        jedisTemplate.execute(new JedisExecuteWithoutReturnCallback() {
            @Override
            public void runInConnection(JedisConnection conn) {
                conn.set(ORDER_QRIMG_PREFIX + orderId, imgPath);
                conn.expire(ORDER_QRIMG_PREFIX + orderId, 20 * 60);
            }
        });
    }

    public String getOrderQrImg(String orderId) {
        return jedisTemplate.execute(new JedisExecuteCallback<String>() {
            @Override
            public String runInConnection(JedisConnection conn) {
                return conn.get(ORDER_QRIMG_PREFIX + orderId);
            }
        });
    }

}
