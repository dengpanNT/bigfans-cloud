package com.bigfans.cartservice.model;

import com.bigfans.Constants;
import com.bigfans.framework.utils.ArithUtils;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @Description:购物车
 * @author lichong
 * 2015年4月4日下午7:14:59
 *
 */
public class Cart implements Serializable{

	private static final long serialVersionUID = -944623908553024249L;
	
	private List<CartItem> items = new ArrayList<>();
	// 总价格
	private BigDecimal totalPrice = BigDecimal.ZERO;
	// 总商品数量
	private Integer totalAmount = 0;
	// 已选择的商品总数
	private Integer selectedTotalAmount = 0;

	@JsonSerialize
	public Integer getTotalAmount(){
		if(totalAmount != 0){
			return totalAmount;
		}
		for (CartItem cartItem : items) {
			totalAmount += cartItem.getQuantity();
		}
		return totalAmount;
	}
	
	@JsonSerialize
	public Integer getSelectedTotalAmount(){
		if(selectedTotalAmount != 0){
			return selectedTotalAmount;
		}
		for (CartItem cartItem : items) {
			if(!cartItem.getIsSelected()){
				continue;
			}
			selectedTotalAmount += cartItem.getQuantity();
		}
		return selectedTotalAmount;
	}
	
	public List<CartItem> getItems() {
		return items;
	}

	public void setItems(List<CartItem> items) {
		this.items = items;
	}

	public BigDecimal getTotalPrice() {
		if(!items.isEmpty()){
			for (CartItem cartItem : items) {
				if(!cartItem.getIsSelected()){
					continue;
				}
				this.totalPrice = ArithUtils.add(this.totalPrice , cartItem.getCurrentSubTotal());
				
			}
		}
		return totalPrice;
	}
}
