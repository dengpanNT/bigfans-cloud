package com.bigfans.userservice.service.impl;

import com.bigfans.userservice.dao.UserCouponDAO;
import com.bigfans.userservice.model.OrderCouponAudit;
import com.bigfans.userservice.model.UserCoupon;
import com.bigfans.userservice.model.UserPointLog;
import com.bigfans.userservice.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service(UserPropertyServiceImpl.BEAN_NAME)
public class UserPropertyServiceImpl implements UserPropertyService {

    public static final String BEAN_NAME = "userPropertyServiceImpl";

    @Autowired
    private UserService userService;
    @Autowired
    private OrderCouponAuditService orderCouponAuditService;
    @Autowired
    private UserPointLogService userPointLogService;
    @Autowired
    private UserCouponDAO userCouponDAO;
    @Autowired
    private UserPropertyService _this;

    @Override
    @Transactional
    public void revertOrderProperty(String orderId) throws Exception {
        _this.revertOrderUsedCoupon(orderId);
        _this.revertOrderUsedPoints(orderId);
        _this.revertOrderUsedBalance(orderId);
    }

    @Override
    @Transactional
    public void useProperty(String userId, String orderId, String couponId, Float points, BigDecimal balance) throws Exception {
        _this.useCoupon(userId, orderId, couponId);
        _this.usePoints(userId, orderId, points);
    }

    @Override
    @Transactional
    public void useCoupon(String userId, String orderId, String couponId) throws Exception {
        int usedCoupon = userCouponDAO.use(userId, couponId);
        if (usedCoupon < 1) {
            throw new Exception("使用优惠劵失败");
        }
        OrderCouponAudit audit = new OrderCouponAudit();
        audit.setUserId(userId);
        audit.setOrderId(orderId);
        audit.setCouponId(couponId);
        audit.setReason(OrderCouponAudit.REASON_ORDERCREATE);
        orderCouponAuditService.create(audit);
    }

    @Override
    @Transactional
    public void usePoints(String userId, String orderId, Float points) throws Exception {
        userService.usePoints(userId, points);
        UserPointLog userPointLog = new UserPointLog();
        userPointLog.setUserId(userId);
        userPointLog.setOrderId(orderId);
        userPointLog.setPoints(points);
        userPointLog.setReason(OrderCouponAudit.REASON_ORDERCREATE);
        userPointLogService.create(userPointLog);
    }

    @Override
    @Transactional
    public void useBalance(String userId, String orderId, BigDecimal balance) throws Exception {

    }

    @Override
    @Transactional
    public void revertOrderUsedCoupon(String orderId) throws Exception {
        OrderCouponAudit couponLog = orderCouponAuditService.getByOrder(orderId);
        _this.addCoupon(couponLog.getUserId() , couponLog.getCouponId() , 1);
    }

    @Override
    @Transactional
    public void revertOrderUsedPoints(String orderId) throws Exception {

    }

    @Override
    @Transactional
    public void revertOrderUsedBalance(String orderId) throws Exception {

    }

    @Override
    @Transactional
    public void addCoupon(String userId, String couponId, Integer count) throws Exception{
        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setUserId(userId);
        userCoupon.setCouponId(couponId);
        userCoupon.setCount(count);
        int ownedCount = userCouponDAO.getOwnedCount(userCoupon.getUserId(), userCoupon.getCouponId());
        if (ownedCount == 0) {
            userCouponDAO.insert(userCoupon);
        } else {
            userCouponDAO.addCount(userCoupon.getUserId(), userCoupon.getCouponId(), userCoupon.getCount());
        }
    }
}
