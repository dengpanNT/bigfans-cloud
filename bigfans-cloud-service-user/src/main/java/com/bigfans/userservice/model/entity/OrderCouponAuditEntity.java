package com.bigfans.userservice.model.entity;

import com.bigfans.framework.model.AbstractModel;
import lombok.Data;

import javax.persistence.Table;

@Data
@Table(name = "OrderCoupon_Log")
public class OrderCouponAuditEntity extends AbstractModel {

    public static final String REASON_ORDERCREATE = "ordercreate";
    public static final String REASON_ORDERREVERT = "orderrevert";

    private String userId;
    private String couponId;
    private String orderId;
    private String direction;
    private String reason;

    @Override
    public String getModule() {
        return "OrderCouponAudit";
    }
}
